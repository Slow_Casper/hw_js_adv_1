class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
};

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);

        this.lang = lang;
    }

    get salary() {
        return super.salary
    }
    set salary(value) {
        this._salary = value *3;
    }
}

const YuriiProgrammer = new Programmer("Yurii", 29, 1000, "ukr");
const AlexProgrammer = new Programmer("Alex", 26, 500, "eng, urk");


console.log(YuriiProgrammer);
console.log(AlexProgrammer);

